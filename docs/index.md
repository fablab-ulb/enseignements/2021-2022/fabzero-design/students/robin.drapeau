## à propos de moi

Bonjour!

Je m'appelle Robin.

![](images/instapp2.png)

J’ai 24 ans et je suis actuellement étudiant en deuxième année de master. En parallèle de mes études, je fais du graphisme et du design, notamment des illustrations, des logos, des faire-part... J’exerce cela en free-lance à mes heures perdues. Je crée des chartes graphiques, mais aussi de l’identité visuelle pour des clients. Je suis avant tout passionné par l’architecture, mais ce que je préfère le plus dans ce domaine c’est l’aspect graphique et visuel de la représentation que ça soit en 2D ou en 3D. Je réfléchis peut-être un jour à m’orienter dans le graphisme après mon diplôme. Ce qui est sur, c’est que je suis quelqu’un de très intéressé, un peu touche à tout. Ci-dessous quelques exemples d'illustrations que j'ai réalisées. Et d'autres disponibles sur mon [site](https://robindrapeaurd.wixsite.com/website), sur mon [instagram](https://www.instagram.com/robindrapeau/?hl=fr) ou sur [youtube](https://www.youtube.com/channel/UCQxB98LzvuGRW-Ce6pfoWnQ).

![](images/illustration.jpg)

## Première idée

La première idée qui m'est venue est celle de retravailler sur un prototype de roller détachable. En effet je suis un grand fan de sport de glisse et à Bruxelles j'aimerais faire plus de rollers. Le problème que je rencontre est que des rollers, ça prend beaucoup de place. Je me suis donc procuré une paire de rollers détachable que je peux mettre directement sur mes chaussures. Bémol : ils prennent encore trop de place et il va être difficile de pouvoir les transporter au quotidien. Je me suis donc mis en tête que je pouvais tenter d'économiser la matière déjà existante dans le but de pouvoir les mettre dans mon sac à dos ainsi que d'autres choses dont j'ai besoin au quotidien comme mon ordinateur.

![](images/rollerdetachable.jpeg)

Après plus d'une semaine à cogiter sur le sujet je me suis ravisé, car il va m'être très difficile d'obtenir un résultat concret et solide. En effet je me suis renseigné sur la solidité des réalisations faites à l'imprimante 3D, le plastique ne sera pas assez solide pour résister à des tensions et des vitesses allant à plus de 30 km/h. Je garde néanmoins ce projet en tête pour l'avenir. Cependant une autre idée, mais venu.

## Deuxième idée

Ma deuxième idée se base sur un de mes passe-temps : le jonglage. Dans le même but que pour les rollers, j'aimerais réaliser des quilles de jonglages qui pourraient se démonter en deux parties. Pour que trois quilles puissent rentrer dans mon sac. L'idée serait donc de séparer la quille en deux parties distinctes et qui pourraient ensuite s'assembler.

![](images/quilles.jpeg)

Ce qui peut être intéressant dans ce travail, ça va être le travail de la matière et de la forme. Pourquoi pas refaire un tout nouveau design ? Mais surtout la partie technique sera l'assemblage des deux parties.
