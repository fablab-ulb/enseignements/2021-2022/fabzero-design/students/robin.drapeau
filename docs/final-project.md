# Final Project : La quille de jonglage

Maintenant que les 5 modules sont terminé je vais pouvoir commencer la documentation sur mon projet final : la quille de jonglage.

## Design or not design?

Victor nous a donné un petit cours sur le design ou il nous a introduit une méthode pour faire du design. Avec quelques références :
- Mon oncle de Tati, film qui caricature du design
- Une approche avec Venturi et l’architecture : Bâtiment avec l’image de ce qu’ils sont, et ils reflètent leur fonction / en opposition avec d’autre qui n’ont aucun rapport
- Kitchen stories

Est-il possible de lier en design, l’outil et le signe ?
Méthode en 6 étapes pour concevoir du design :

#### 1 / Déconstruction de la fonction

Regard détacher avec un autre regard, sans a priori.
Vous êtes un simple observateur.
Soyez curieux, méthodique et précis.
Vous analysez et comprenez l’objet, son utilité, ses problèmes.

#### 2 / Résoudre un vrai problème

Période essentielle de mise en place des limites / contraintes.
Les contraintes sont écrites dans un objet.
Elles dont l’objet.
Elles sont structurantes à la créativité.
Elles permettent de regarder son travail en impitoyable.

#### 3 / Comprendre les dimensions spécifiques

Penser en ergonome.
Au confort d’utilisation, dimensionnement du corps.
Pensez aux gestes, au poids.
Chaque objet possède ses propres règles de dimensions.
Il faut les connaître pour faire un objet adapté à l’’utilisateur.

#### 4 / Esquisse, essai-erreur, amélioration

Faire et refaire, tester, s’enrichir d’avis extérieurs.
Décision dure la technique / avec les machines disponibles.
Faire une boucle si nécessaire vers les étapes précédentes
Prototypes sales, Prototypes propres, Prototypes fonctionnels.

#### 5 / Adaptation, diffusion édition

Mon objet est unique, il peut être étendu à d’autre et reproduisiez.
Question de la série, de la collection un objet est rarement seul.
Il est adapté ou décliné pour sa diffusion pour un plus grand nombre.
Je le réduis sa plus simple expression au minimum de matière.
Il est documenté, possibilité de le reproduire avec son mode d’emploi.

#### 6 / Un nom, un pictogramme

Qui le rend lisible vite et facilement.
Le rends compréhensible.
Le mets dans un contexte et le lie à un univers.

D'autres références de design :

```
Cuitochète, Litière pour chat, La corde pour s’asseoir, Le pouf un siège sans forme
Le vélo Cowboy, Chaise de Ims, meuble en bois a la cnc Open desk
```

## Étape 1 : Déconstruction de la fonction

- Tout d'abord la quille de jonglage, *qu'est ce que c'est ?*

La massue, parfois quille de bois ou torche, est un des accessoires traditionnels de la jonglerie aérienne avec les balles et les anneaux. Elle a la particularité de pouvoir effectuer une ou plusieurs rotations dans les airs lorsqu’elle est lancée, ce qui en fait un agrès particulièrement spectaculaire lorsqu’il est utilisé pour les passing à plusieurs jongleurs.

- Une massue se compose de quatre parties :

Le top, en mousse plastique compressée, est la partie supérieure de la massue. Souvent noir ou blanc, il sert à amortir les chutes, essentiellement pour protéger la baguette de bois constituant l’armature intérieure (appelée âme)

Le corps, généralement en plastique, est la partie plus grosse de la massue. Il sert de balancier pour la rotation de la massue. La taille et la forme du corps font varier la vitesse de rotation ;

Le manche est la partie la plus fine, très souvent blanche ou de couleur métallique. C’est l’endroit par lequel il faut saisir la massue. Il existe des massues à manche courte ou longue selon la pratique désirée : jonglage seul ou passing.

Le bouchon est la partie inférieure de la massue. Comme le top, il est souvent noir ou blanc et sert à amortir les chutes, mais peut aussi servir à faire des figures telles que les balances et arrêts sur les points d’équilibres.

Le choix de la tige en bois est très important : l’essentiel est qu’il soit homogène en densité, car le poids de cette pièce est le seul inconvénient à la production pour obtenir des massues identiques. Le contrôle qualité est donc très rigoureux à cette étape. La baguette est un tourillon d’un demi-centimètre de diamètre et de 46 à 50 cm de long pour les modèles standards. Pour éviter la casse de cette pièce vitale, il faut que la pièce soit parfaitement taillée dans le « fil du bois », qu’elle ne bande pas et ne comporte aucun nœud. Il y a beaucoup de perte à ce niveau — plus de 75 %. Les pièces ne respectant pas ces contraintes peuvent servir à fabriquer d’autres ustensiles comme des baguettes de diabolo par exemple. Bouchons et tops doivent être changés une fois usés pour protéger la massue des chocs.

![](imagesfinalprojet/quilleplein.jpg)
![](imagesfinalprojet/quilledemonte.jpg)
![](imagesfinalprojet/quillezoom.jpg)

- Les différentes massues de jonglage :

Il existe différentes massues de jonglage, je vais les énumérer et les illustrer ci-dessous.

Massue de jonglage en bois :
Les plus anciennes massues, assez lourde maintenant on en utilise de moins en moins de cette sorte.

![](images/massuebois.jpg)

Massue de jonglage en plastique :
Les plus classiques et les plus courantes. Entièrement recouverte de  plastique avec à l'intérieur une tige en bois pour les rigidifier.

![](images/massueplastique.jpg)

Massue de jonglage en feu :
L'extrémité de la massue est en métal pour résister au feu et le bout est une sorte de toile de jute qui permet d'absorber l'alcool et par la suite pour l'enflammer.

![](images/massuetorche.jpg)

Massue de jonglage lumineuse :
Elles aussi sont en plastique, mais à l'intérieur on peut retrouver une LED qui diffuse la lumière dans toute la quille.

![](images/massuelumieuse.jpg)

## Étape 2 : Résoudre un vrai problème

- Problème 1 :

Le premier qui me dérange le plus c'est lorsque je me déplace avec mes quilles de jonglage, je les trouve trop volumineuses, j'entends par là qu'elles dépassent de mon sac. Et c'est fort encombrant (ça prend la moitié de mon sac à dos). L'idée serait de diminuer drastiquement la taille de ces quilles.

*Comment ?*

Soit en divisant en différentes parties la massue de jonglage. Pour optimiser son rangement. Et pourquoi pas rentrer des pièces dans d'autres, dans le but de compacter un maximum l'objet.

- Problème 2 :

De plus il faut prendre en compte la légèreté et la solidité de la quille qui n'est pas à négliger, car elle doit être parfaitement équilibrée et résister aux éventuelles chutes.

- Problème 3 :

L'idée serait d'avoir une quille de jonglage modulable. Pourquoi ? Car dans le commerce on ne trouve pas quille de jonglage avec différentes fonctions interchangeables. Le but serait de changer la partie supérieure de la massue et mettre différentes fonctions en son bout.

*Quelle fonction ?*

Il y en a 3 que je voudrais mettre en place. La première serait une tête classique pour simplement jongler et pouvoir juste s'entraîner tranquillement. La deuxième serait une tête lumineuse pour pouvoir jongler dans le noir. Et la dernière, pour les mêmes raisons, une torche en feu. Pouvoir enflammer le bout et jongler avec. (PS Je n’ai encore jamais jonglé avec du feu, mais il y a un début à tout).

## Étape 3 : Comprendre les dimensions spécifiques

Les dimensions d'une quille de jonglage sont assez souvent les mêmes et peuvent varier légèrement si l'on veut garder les propriétés pour la jonglerie. Longueur 52 cm. Même chose pour le poids, il est habituellement autour de 225 gr. Il faut prendre en compte que la massue est équilibrée et le poids est réparti de bout en bout avec une forme galbée qui permet de garder la force de rotation continue lorsqu'elle est en l'air et en mouvement.

La particularité de la quille de jonglage, c'est au niveau de sa composition. À l'intérieur se trouve une tige en bois, qui permet une tension de bout en bout de la massue. Et aussi elle rigidifie la massue dans son ensemble.

## Étape 4 : Esquisse, essai-erreur, amélioration

### Premier test à l'imprimante 3D

Le but des tests que je vais réaliser va me permettre de voir s'il est possible de faire un système de filetage pour séparer mes différentes pièces de la massue de jonglage.

#### Test de filetage

Deuxième test d'impression pour tester le filetage sur fusion mais aussi voir ce que cela donne une fois imprimé en 3D. Sur ce test, la vis ne rentre pas dans l'autre pièce. Je vais donc effectuer des réglages sur Prusasciler et sur fusion 360.

![](imagesfinalprojet/impression2.png)

Troisième test d'impression, celui ci fonctionne après avoir changé quelque paramètre que j'ai déjà expliqué dans le module 3 sur l'impression 3D.

![](imagesfinalprojet/impression3.png)

Le fichier STL du troisième [test](imagesfinalprojet/test3.stl)


Les tests de filetages sont concluants, les deux pièces rentrent parfaitement l'une dans l'autre. Je vais donc pouvoir garder ce type de filetage pour assembler les différentes parties de la quille de jonglage.

#### Dessin de la quille

Ci-dessous un dessin qui permet de comprendre comment les différentes parties de la quille de jonglage seront séparées. Ou ce trouvent les différentes sections et comment elles s'assemblent.

![](imagesfinalprojet/dessin1.png)

Ci-dessous les différentes cotations de la quille de jonglage.

![](imagesfinalprojet/dessin2.png)

#### Impression de la partie 1

Je commence par imprimer le bas de la quille de jonglage, en commençant par le manche.

![](imagesfinalprojet/bas1.png)

J'ai eu un souci pour l'impression sur une surface du manche (celle qui était sur les supports). Pour remédier à cela lors de mon prochain test, je la réaliserais en deux parties. Et la vis permettra d'assembler le manche. Car le manche étant trop grand je ne peux pas l'imprimer sur la hauteur, je suis obligé de la coucher.

Ci-dessous un screen de mon fichier fusion pour donner une idée de la modélisation.

![](imagesfinalprojet/fusionbas.png)


#### Impression de la partie 2

Je viens de lancer l'impression de la première partie du haut de la quille : celle qui va pouvoir se viser dans le manche.

![](imagesfinalprojet/impr2.png)

Ci-dessous un screen de la pièce qui va être imprimé :

![](imagesfinalprojet/fusionhaut1.png)

Ci-dessous un screen du fichier fusion avec toutes les parties du haut de la quille :

![](imagesfinalprojet/fusionall.png)

#### Impression de toutes les parties

![](imagesfinalprojet/detache.jpg)

Maintenant que toutes les parties de la quille sont imprimées, je vais pouvoir les assembler pour réaliser la quille dans son ensemble.

![](imagesfinalprojet/monte.jpg)

'ai donc pu réaliser différents tests pour observer comment la quille réagit lorsque je la lance en l'air en effectuant un tour.

J'ai rencontré deux problèmes :

- le premier c'est la répartition du poids qui n'est pas la même que celle d'une quille de jonglage classique.
- le deuxième est la solidité de la quille, en effet le fait qu'elle soit imprimée en plastique, couche par couche, fait que la quille, au moindre choque finira par casser au niveau des jonctions.

### Amélioration

#### Tableau des caractéristiques

| Quille |  Poids    |  Centre de masse  |
|-----|-----------------|---------|
| Classique   | 206 g |  légèrement sur le haut|
| Impression 3D   | 265 g  |  davantage sur le haut|
| Le bâton   | 170 g |  au centre du bâton|

#### Essaie / test

J'ai réalisé des tests pour observer comment les différentes quilles réagissent lorsque je les lance et qu'elles effectuent un tour complet en l'air. J'ai filmé avec Denis chaque lancer dans le but de voir comment le centre de masse influence la trajectoire de la quille en l'air.

![](imagesfinalprojet/tape.jpg)

![](video_quille/baton.mov)

Ci-dessous un schéma correspondant au mouvement de la quille de jonglage classique. En observant en jaune le centre de masse, en rouge et vert les extrémités de la quille.

![](imagesfinalprojet/schema.png)

Je continue en repartant sur le principe même de la quille de jonglage. C'est-à-dire une barre avec des poids aux extrémités. J'utilise donc deux types de tiges, l'une fileter en métal et l'autre lisse en aluminium donc plus léger et où je pourrais plus facilement contrôler les poids aux extrémités ainsi que le centre de masse.

![](imagesfinalprojet/tige.jpg)

### Prototype 2

En partant sur l'idée d'une quille de jongle sur un axe fileter, je vais pouvoir modifier le centre de masse comme bon me semble. La quille sera donc adaptable à chaque jongleur et en fonction de ce qu'il désire en faire.

#### Le matériel

Pour réaliser une quille, j'aurais besoin d'une tige filetée de 50cm de long de diamètre M6, de 12 boulons M6, ainsi que 8 rondelles.
![](imagesfinalprojet/rondel.jpg)
![](imagesfinalprojet/tigef.jpg)

###### Le manche

Je commence par réaliser un manche à l'impression 3D. Celui-ci devra laisser passer la tige filetée à l'intérieur. Je l'ai réalisé sur fusion puis je l'ai imprimé en 3D.
Il est séparé en deux parties pour pouvoir l'imprimer en format vertical, ci-dessous le rendu 3D de ce à quoi il devrait ressembler.

![](imagesfinalprojet/manchestl.png)

Ci-dessous l'impression finit

![](imagesfinalprojet/manchetrou.jpg)

###### Le moule

Ensuite je procède à la conception du moule qui me permettra de réaliser les différentes pièces qui formeront la quille et qui définiront la forme de celle-ci.
L'objectif est de concevoir un moule qui reproduira le même module pour chacune des quilles. Je passe donc sur fusion pour le faire. Je m'inspire de la forme de bas d'une quille de jonglage classique.
Je laisse deux trous, un pour laisser passer le liquide (silicone ou caoutchouc liquide) et l'autre pour faire passer la tige filetée ainsi que trois boulons et deux écrous avant d'insérer le liquide.
Puis j'imprime les différentes parties à l'impression 3D qu'on peut voir ci-dessous.

- test 1

Le premier test est un échec, l'impression à rater sur l'intérieur d'un moule. De plus les dimensions n'étaient pas totalement justes, un peu trop grandes.

![](imagesfinalprojet/moulefail.jpg)

- test 2

Cette fois-ci, je change quelques paramètres sur fusion et sur prusaslicer. Et je relance l'impression 3D.

![](imagesfinalprojet/moule1.jpg)
![](imagesfinalprojet/moule2.jpg)

###### Module 1 : le caoutchouc liquide

J'ai acheté une poudre de caoutchouc chez Shleipper (20Euros) qui suffit de diluer avec de l'eau pour en faire une sorte de slime qui deviendra du caoutchouc comme on le connaît.

![](imagesfinalprojet/caoutch.jpg)

Je réalise un premier mélange de 1:1 (eau:poudre) qui n'est pas concluant, car il était trop solide. Ci-dessous le premier test.

![](imagesfinalprojet/caou1.jpg)

Puis je réalise un deuxième mélange avec cette fois-ci plus d'eau, 2:1 (eau:poudre). Cette fois-ci le test semble concluant, malheureusement la photo ci-dessous est prise 2 jours après et l'eau a continué à "transpirer" et donc le module est devenu plus friable et plus petit.

![](imagesfinalprojet/caou2.jpg)

###### Module 2 : Le silicone

Je réalise donc un autre test. Cette fois-ci en accord avec Denis je teste avec un silicone de chez mold star. Je mélange donc la partie A avec la B. Puis j'insère le liquide à l'intérieur du moule.

![](imagesfinalprojet/silicone.jpg)

Le résultat est très satisfaisant, la photo du module ci-dessous.

![](imagesfinalprojet/module2.jpg)
En comparaison avec la taille du premier test.
![](imagesfinalprojet/compar.jpg)

J'en réalise donc d'autre pour obtenir 3 modules a placer sur la quille comme bon me le semble.

###### Protection de la tige

Lors d'essai de jonglage, je me suis rendu compte qu'il fallait protéger la tige filetée pour les éventuelles chutes. Car si la tige est abîmée je ne pourrais plus faire coulisser les modules le long.

J'ai donc réfléchi à une protection pour la protéger. Je me suis dit qu’un système de bague ferait l'affaire. Et comme matière le PLA sera suffisamment résistant.

Je me lance donc sur Fusion 360 et je modélise la bague. Puis l'imprime en 3D. 1,5 cm de haut.

![](imagesfinalprojet/cache.jpg)
![](imagesfinalprojet/cache2.jpg)

###### Module 3 : Un autre silicone

Enfin après avoir durement cherché le Silicone Moldstar, je me suis rabattu sur un autre silicone. Toujours dans le même moule j'effectue un troisième test.

Ci-dessous une photo en attendant que le silicone sèche. (la pâte à modeler sert de joint pour éviter que le silicone liquide s'échappe).

![](imagesfinalprojet/silicon.jpg)

Le résultat une fois démoulé

![](imagesfinalprojet/newsilcon.jpg)

#### Résultat

La quille ressemble donc à cela une fois monté. Elle est fonctionnelle. Mais reste un peu lourde. Je pense qu'il faudrait que je trouve un autre système que la tige filetée métallique, car de un elle est lourde et de deux je prends trop de temps à déplacer les modules le long.

![](imagesfinalprojet/final.jpg)
