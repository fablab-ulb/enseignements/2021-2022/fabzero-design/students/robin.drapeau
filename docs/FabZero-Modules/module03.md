# 3. Impression 3D

Cette semaine, après avoir pris connaissance de Fusion 360 la semaine passée, on va s'attaquer à l'impression 3D.

## Impression 3D

Télécharger [Prusa](https://www.prusa3d.com/drivers/) pour commencer le module 3

### Étape 1 : Importer le modèle

Il faut commencer par importer le modèle en .stl

![](screenmodule3/importer.png)

La quille que j'ai réalisée est trop grande pour l'imprimer sur l'imprimante 3D donc je vais réaliser une réplique en miniature pour comprendre comment fonctionne l'imprimante 3D ainsi que le logiciel prusa.

![](screenmodule3/taille.png)

### Étape 2 : Rétrécir le modèle

Avec l'outil échelle à droite de l'écran, je sélectionne les poignées orange pour redéfinir la taille de mon objet sans changer les proportions.

![](screenmodule3/retrecir.png)

### Étape 3 : Régalge de l'impression

Maintenant je règle les paramètres d'impression comme le remplissage, la jupe, la bordure, et les supports.

![](screenmodule3/remplissage.png)
![](screenmodule3/bordure.png)
![](screenmodule3/support.png)

J'obtiens une impression qui devrait durer 1h32. Il me reste plus qu'à exporter le G-code et le mettre sur une carte SD.

![](screenmodule3/final.png)

J'ai pu tester les différents outils de prusa. cependant je ne vais pas imprimer une version miniature de la quille. Mais plutôt une pièce.

## Impression de la pièce

### Fusion

Je vais imprimer en 3D la pièce ci-dessous. Elle correspond à la pièce du bas de la quille de jonglage. J'ai réalisé un filetage que j'ai modifié un peu pour essayer que les pièces coïncident. C'est un premier test de filetage.

![](screenmodule3/piece/piecefusion.png)

### PrusaSlicer

Je l'importe dur prusa3D et je règle les paramètres suivants (couches, bordure, jupe, remplissage).

![](screenmodule3/piece/bordure.png)
![](screenmodule3/piece/couche.png)
![](screenmodule3/piece/remplissage.png)
![](screenmodule3/piece/apercu.png)

Puis j'exporte le G-code, et je le mets sur la carte SD. Et je lance l'impression.

### Photos

#### Premier test

Le test du prototype a échoué. Le problème que j'ai rencontré était dû à la vitesse et la taille de la buse. En effet j'ai voulu aller un peu trop vite donc la vis ne rentrait pas dans la pièce du bas.  

![](screenmodule3/impression2.png)

#### Deuxième test

J'ai donc changé la vitesse et la taille d'impression des couches : je suis passé de 150% à 100% de vitesse / et les couches de 0,2 à 0,05. La pièce à été imprimé de manière beaucoup plus précise. Cependant j'ai changer aussi un paramètre dans fusion. J'avais au préalable rendu la taille du pas de vis en dimension paramétrique. Donc pour augmenter le jeu entre les deux pièces, j'ai augmenter de 0,2mm l'espace entre les deux pièces.
Cette fois-ci, la vis rentre parfaitement dans l'autre partie.

![](screenmodule3/impression3.png)

## Liens modèles 3D

- [piece.stl](fichiers/piecequille.stl)
- [piece.f3d](fichiers/piecequille.f3d)
