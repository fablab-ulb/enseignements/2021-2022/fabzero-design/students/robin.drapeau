# 2. Conception Assistée par Ordinateur

Cette semaine on va apprendre à utiliser fusion, et modéliser un objet sur fusion.

## Fusion 360

Télécharger [Fusion](https://www.autodesk.fr/products/fusion-360/free-trial) pour commencer le module 2

### Les bases de fusion

On commence par le premier cours où l'on nous apprend les bases de fusion.
On procède par un test, la création du cube du fab lab : pour apprendre les différents outils de Fusion. Avec plusieurs captures d'écran je vais répertorier et documenter toutes les étapes de la conception du cube.

- Créer un carré

Avec l'outil esquisse on clique sur un plan puis à l'aide de l'outil rectangle on trace un carré de 10 par 10 avec comme centre l'origine.

- Créer un cube

Avec l'outil extrusion on extrude 5 de chaque côté donc 10 pour obtenir un cube de 10 par 10 par 10.

- Arrondir les bords

Avec l'outil congé on arrondit les arêtes.

- Faire un percement

On effectue ensuite un premier percement en créant l'esquisse d'un cercle puis on extrude le cercle à l'intérieur, on obtient un cylindre creux.

![](screenmodule2/part1.png)

- Dupliquer le percement

On duplique la fonction de percement en fonction d'un axe. Et on répète encore une fois l'expérience.

- Créer un rectangle à extraire

Même chose pour enlever la surface d'un rectangle à l'intérieur. On créer une esquisse d'un carré puis on l'extrait du volume déjà existant. Et on duplique l'opération en fonction d'un axe que l'on a préalablement créé.

![](screenmodule2/part2.png)

### Mon objet : La quille de jonglage

![](screenmodule2/quillephoto.png)

Comme premier exercice sur mon objet. Je vais commencer par réaliser l'objet sur Fusion 360. Je commence par scinder les étapes en fonction des différentes pièces. Les trois jointures : celle du bas, celle du milieu (qui relie le manche du haut), et celle du haut. Il y a aussi le manche et enfin la partie évasés en haut.

#### Première étape : créer la pièce du bas

- Créer un cercle

Je commence par faire un cercle de 22mm de diamètre

- Extrusion

Ensuite j'extrude le cercle de 20mm

- Congé

J''utilise l'outil congé pour arrondir le bas de la pièce
Puis je répète l'opération pour le haut de la pièce
![](screenmodule2/quille1.png)

#### Deuxième étape : créer la pièce du milieu

- Créer un autre plan

Je commence par créer un plan parallèle à la pièce du bas. Je lui attribue une valeur paramétrique (hauteurmanche = 190mm)

- Cercle

Sur ce nouveau plan je crée une esquisse de cercle. En prenant comme diamètre celui que j'ai prélevé avec un pied à coulisse sur la quille.

- Extrusion

J'extrude le cercle. Puis je réalise un autre cercle à l'intérieur que j'extrude pour créer une épaisseur.

![](screenmodule2/quille2.png)

- Créer un autre plan

Comme précédemment, je commence par créer un plan parallèle à la pièce du milieu. Je lui attribue une valeur paramétrique (hauteurquille = 260mm)

- Cercle

Sur ce nouveau plan, je crée un premier cercle d'esquisse. En prenant comme diamètre celui que j'ai prélevé avec un pied à coulisse sur la quille. Puis un deuxième cercle créé sur un plan 2cm au-dessus et avec un diamètre inférieur.

- Lissage

Je sélectionne les deux cercles et j'utilise l'outil lissage qui va créer le volume que je recherche pour la pièce du haut.

![](screenmodule2/quille3.png)

- Lissage du manche

Je procède à un lissage entre la pièce du bas et celle du milieu pour former le manche de la quille.

- Création du profil de la quille

Sur un plan vertical, je crée une esquisse du profil de la quille. Avec l'outil d'esquisse : ligne.

- Révolution

Avec l'esquisse que je viens de réaliser, je vais pouvoir faire une révolution avec l'outil révolution. Le volume correspond à la forme du profil d'esquisse qui se duplique en fonction de l'axe central.

![](screenmodule2/quille4.png)

Voilà la quille vient d'être modélisé en 3D. Objectif atteint lors du module 2.

## Liens modèles 3D

- [cube.stl](fichiers/Testv1.stl)
- [cube.f3d](fichiers/Testv1.f3d)
- [quille.stl](fichiers/Quillev3.stl)
- [quille.f3d](fichiers/Quillev3.f3d)

les liens ci-dessus pour télécharger le fichier .STL et .f3d
