# 4. Découpe assistée par ordinateur

Cette semaine on va apprendre à utiliser les découpeuses laser.

## Utilisation des machines

Tout d'abord, quelques règles à prendre en compte lors de l'utilisation des machines. Il faut d'abord prendre en compte que le fichier que l'on possède sur l'ordinateur doit être vectoriel pour réaliser la découpe, de préférence des fichiers SVG. Le principe d'un fichier vectoriel est que les tracés ne perdent pas, en qualité peu importe à quel point on zoome dessus. Que l'on peut différencier avec un dessin matriciel qui constitué de pixels.

### Les matériaux

##### Recommandés

- bois contreplaqué de 3 ou 4 mm idéalement
- carton
- papier
- acrylique
- certains textiles

##### Déconseillés

- MDF
- ABS, PS
- les métaux
- polyéthylène épais PE, PET, PP
- les matériaux composites à base de fibre

##### Interdits

- Téflon
- le PVC
- le cuivre
- le vinyle ou le similicuir
- Résine phénolique, époxy

### Les machines

![](screenmodule4/machines.png)

#### *Machine 1 : Epilog Fusion Pro 32*

Il s'agit d'une machine conçue en usine. Elle est bien plus petite que la Lasersaur donc utilisée plus pour des petites pièces et principalement de la gravure. Elle a une surface de travail de 50x30cm, un laser de 40 watts.

[Guide d'utilisation](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/EpilogFusion.md)

#### *Machine 2 : Lasersaur*

Il s'agit d'une machine en open source que le Fab Lab a construit lui-même. Elle permet de travailler sur une plus grande surface que la première machine et faire des découpes de qualité de par sa puissance. Avec un espace de découpe de 122x61 cm et une hauteur  de 12 cm. Elle peut monter à une vitesse maximale de 6000 mm/min. La puissance de son laser est de 100 Watts infrarouges. Cette interface gère les fichiers SVG ou DXF. Avec une préférence pour les fichiers SVG. S'il y a un problème, on peut passer sur INKSCAPE (c'est la version gratuite en termes de logiciel vectoriel) pour le régler.

[Guide d'utilisation](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/Lasersaur.md)



## Exercice 1 : Réaliser un échantillon

#### Tracé vectoriel

On commence par tracer sur un logiciel vectoriel l'échantillon que l'on vouloir découper. Dans notre cas on à réaliser, en groupe une bandelette ou dessus on garde une puissance constante (10Watt), mais on varie la vitesse. Allant de 100 à 2300. L'objectif est de réaliser par la suite de pliage lors du deuxième exercice. Donc il est intéressant de tester différentes gravures sur le plastique pour obtenir le pliage que l'on recherche.

Ci-dessous la bande réalisée sur illustrator. (on a rencontré un problème sur la lasersaur, car on n’avait pas changé les couleurs en fonction des différents traits à graver/découper donc on à changer les couleurs par la suite)

![](screenmodule4/polytestpliage.png)

#### La découpe

On passe ensuite notre fichier en [SVG](screenmodule4/pliage.svg) et on le met sur une clef USB pour le mettre sur la lasersaur. On ouvre le logiciel de découpe et on importe notre fichier svg. Dans le logiciel de découpe, on règle les différents paramètres : on laisse la puissance pour chaque trait de 10Watt. Mais on chaque trait la vitesse (de 100 à 2300). Puis on lance la découpe, et on obtient le résultat ci-dessous.

![](screenmodule4/echant.png)

## Exercice 2 : Réaliser l'abat-jour d'une lampe

#### Premier test : Emboîtement

La suite du module consiste à réaliser un abat-jour pour une source lumineuse. Dans mon cas je vais relier cet exercice à mon idée de projet initial : la quille de jonglage. Je vais donc tenter de réaliser une quille de jonglage lumineuse. Pour cet exercice on a seulement le droit d'utiliser une planche de polypropylène qui mesure 71x51 cm.

![](screenmodule4/pliage1.png)

L'idée de la quille de jonglage est bonne, mais je ne pourrais pas en réaliser 3 sur une seule planche donc je vais plutôt essayer faire des balles de jonglage.
Je commence donc par acheter des LED sur piles. Car la taille de la balle dépendra de la taille de la source lumineuse.

![](screenmodule4/photoled.png)

Je réalise ensuite un premier prototype en carton pour comprendre la dimension de la sphère que je vais réaliser.

![](screenmodule4/protocarton.png)

Puis je fais les tracés sur illustrator (j'aurais aussi pu le faire sur autocad ou inkscape). Je duplique ma première balle en 3 pour en faire trois. Malheureusement je test différentes compositions pour tenter de faire rentrer les trois balles dans la planche de polypropylène, mais je n'arrive pas à les faire rentrer. Je vais donc avoir besoin d'une autre planche pour réaliser la fin de la troisième balle.

![](screenmodule4/boulecol.jpg)
![](screenmodule4/boulenb.jpg)

Il me reste encore à découper ça au laser. J'exporte mon fichier en SVG et le mets sur une clef USB.

#### Deuxième test : Les lamelles

- prototype 1

Changement de plan, le système d'emboîtement n'allait pas résister aux potentielles chutes. J'ai donc rerflechi à un système qui amortirait davantage la chute. Je me suis reconcentré sur des balles en lamelle.
Lors de mon premier prototypage, je me suis adapté par rapport à la taille du boîtier de la source lumineuse.

Je me suis donc mis sur illustrator pour dessiner des bandelettes. Puis je les découper à la lasersaur. Et assemblé. Une photo ci-dessous du premier prototype.

![](screenmodule4/grande.jpg)

- Boîtier en 3D

J'ai réalisé un plus petit prototype par la suite, car avec le premier il était trop difficile de jongler avec donc je me suis penché sur une solution pour réduire la taille de la source lumineuse.
J'ai donc commencé par enlever le boîtier d'origine et garder que les fils. Ensuite je suis parti dans l'optique de diminuer la taille des piles donc j'ai pris trois piles plates de 1,5V à la place des trois piles AA de 1,5V également. J'ai ensuite réalisé un boîtier sur fusion 360 ou je pouvais maintenir les câbles et les piles de la LED. Puis je l'ai imprimé en PLA à l'aide des imprimantes 3D et de Prusaslicer.

![](screenmodule4/boitier3d.png)

Ensuite j'ai repris mon modèle illustrator pour mes languettes et j'ai réduit la taille pour réaliser une balle de jonglage plus conforme. Et j'ai redécoupé ça à la lasersaur.

![](screenmodule4/screenlang.png)

![](screenmodule4/languettte.jpg)

Puis j'ai assemblé les languettes et j'ai formé la balle. Et j'y ai introduit la source lumineuse. Le résultat obtenu bien meilleur.

![](screenmodule4/petite.jpg)

![](screenmodule4/ballefinal.jpg)

Voilà le résultat à côté des différents projets.

![](screenmodule4/table.jpg)

J'ai par la suite répété l'opération pour en réaliser deux autres. Je peux enfin jongler avec les trois balles lumineuses : Objectif atteint.

![](screenmodule4/troisballe.jpg)

## Liens

- [languette.svg](fichiers/languette.svg)
- [boitierpile.stl](fichiers/pilev2.stl)
