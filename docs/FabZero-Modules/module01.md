# 1. Documentation Gitlab

Cette semaine je me suis penché sur le procédé de documentation sur Gitlab. Lors de ce module 1 je vais documenter étape par étape comment je suis arrivé à la fin de ce module avec pour objectif : pouvoir push mon travail depuis ATOM.

## Déroulement des étapes de la documentation

### Première étape : Installation de Git.


- Tout d’abord j'ai télécharger le logiciel Gitlab qui permet donc de travailler en groupe sur un même projet et de documenter tout nos travaux.
- J’ai ouvert le terminal de mon ordinateur, je travail sur Mac OS, j’ai donc appuyé sur command + space et j’ai écrit terminal qui est déjà installé sur mac.

- Je procède de la manière suivante en commençant par le plus compliqué ; l'installation de la clef SSH. Pour cela il faut d'abord installer GIT. En suivi les instructions que l'on trouve dans le fichier [Documentation.md](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#configure-git). Étant sur IOS je procède à l’installation de [HOMEBREW](https://brew.sh/index_fr). Sauf que je rencontre déjà un problème lorsque j’entre la commande dans le terminal :
```
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```

- J’ai en suite télécharger XCode en encodant  xcode-select —install dans le terminal. J'ai pris un screenshot du terminal pour le répertorier. Il fallait que je fasse la mise à jour de mon ordinateur ainsi que la mise à jour de Xcode pour pouvoir installer [HOMEBREW](https://brew.sh/index_fr). Après cela j'ai réussi à l'installer, enfin.
![](screenmodule1/Bughomebrew.png)
![](screenmodule1/Homebrewinstal.png)

###	Deuxième étape : Configurer Git.

Je procède à la configuration de Git, en changeant mon nom et mon adresse mail, et je vérifie si cela a bien fonctionné avec la commande :
```
git config --global –list
```
Ci-dessous des captures d'écran de la configuration de Git.
![](screenmodule1/Conofigit1.png)
![](screenmodule1/Conofigit2.png)

###	Troisième étape : Fork et clone

Je créé une fourche du projet exemple, firstname.surname en cliquant sur fork en haut à droite du projet.

### Quatrième étape : la clef SSH

- Je paramètre ma clef SSH pour lier le dossier de mon ordinateur avec celui du Gitlab. Grâce à cela je pourrais réaliser des modifications depuis mon ordinateur en serveur local et par la suite l'upload sur gitlab en faisant un push. J'ai suivi les instructions de ce [DOC](https://docs.gitlab.com/ee/ssh/index.html#ed25519-ssh-keys)
![](screenmodule1/clefsshcreate.png)
![](screenmodule1/clefssh.png)

### Cinquième étape : installation d'ATOM

Pour pouvoir travailler directement en local depuis mon ordinateur et écrire ce que je suis en train d'écrire j'ai installé le logiciel Atom directement depuis le terminal en seulement quelques lignes de code que j'ai répertorié ci-dessous.
![](screenmodule1/instalatom.png)

### Sixième étape : Push

![](screenmodule1/testpush.png)
![](screenmodule1/gitpush.png)

Voilà à partir de maintenant je vais pouvoir travail sur Atom et push mes modifications depuis Atom en seulement quelques commandes.

## Liens

- [Markdown](https://en.wikipedia.org/wiki/Markdown)
- [GIT LAB](https://about.gitlab.com/fr-fr/)
