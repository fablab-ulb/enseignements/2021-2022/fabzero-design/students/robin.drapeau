# 5. Usinage assisté par ordinateur

Lors de cette formation on nous a appris comment fonctionne et comment utiliser la Shaper.

## La Machine

La [Shaper Origin](https://www.shapertools.com/fr-fr/origin/spec) est une CNC portative de précision, guidée à la main. Elle permet de découper des pièces ou bien d'éxtruder de la matière de manière très précise grace à la machine elle même qui corrige la trajectoire.
Le manuel d'utilisation est [ici](https://assets.shapertools.com/manual/Shaper_Origin_Product_Manual.pdf).

![](screenmodule5/shper.jpg)

##### Informations

- Profondeur de découpe max. : 43 mm
- Diamètre du collet : 8 mm ou 1/8”
- Format de fichier supporté : SVG

##### Précautions d’usage

- Utiliser la machine sur un plan de travail stable.
- Fixer le matériau à découper sur le plan de travail (double face, serre-joint ou vis).
- Allumer l’aspirateur à poussières.
- Être dans une position stable, équilibrée et confortable pour l’utiliser.
- Prendre en compte le fait que la découpe soit à notre portée sur tout le long de l'opération.
- Éviter tout ce qui peut ce coincer dans la shaper (vêtements larges, bijoux, cheveux longs lâchés).

### Reglages

##### Vitesse

De 1 à 6 suivant l'épaisseur et le matériau que l'on va vouloir découper (10 000 à 26 000 tours/minute). Il faut éviter de bruler le bois en voulant extraire une partie trop épaisse d'un coup.

##### Différentes découpes

- inside : découpe à l’intérieur du tracé.
- outside : découpe à l’extérieur du tracé.
- on line : découpe sur le tracé.
- pocket : coupe par évidage.

## L'utilisation

### Fichier source

Tout d'abord sur un logiciel vectoriel, il faut d'abord dessiner un motif que l'on va devoir exporter obligatoirement en svg s'il l'on que cela fonctionne.
Il faudra faire bien attention à ce que les tracés soit bien fermé car sinon la machine ne reconnaitra pas le tracé.

### La découpe

##### 1 / Shaper tape

Commencer par placer le shaper tape, il servira de guide pour la shaper. En effet la Shaper Origin possède une caméra qui détecte le shaper tape et grâce à celle là, elle va pouvoir se calibrer et corriger les mouvement manuel.

![](screenmodule5/tape1.jpg)

Le placer en bandes parallèles séparées de 8cm maximum et de minimum 4 rectangles (domino) et on met la fraise adaptée dans la machine.

![](screenmodule5/tape.jpg)

##### 2 / La fraise

Pour changer la fraise, il faut tout d’abord appuyer le bouton de verrouillage de la broche, puis dévisser la vis à l’aide de la clé T-Hexagonale afin de retirer la broche.

![](screenmodule5/foret.jpg)

##### 3 / Scanner

Scanner la zone de travail avec la machine. L’indicateur en forme de domino en haut à droite de l'écran permet de vérifier que toute la zone de découpe a suffisamment de Shaper tape. Si c'est ok, on peut importer le fichier source.

##### 4 / Stabilité et accessibilité

Pour finir la préparation, on place le dessin sur la zone de découpe et on revérifie que toute la zone est accessible de façon stable et couverte par les repères visuels.

##### 5 / L'aspirateur

Avant d’effectuer la découpe, il ne faut pas oublier d’allumer l’aspirateur à poussières et de le relier à la machine à l’aide du tuyau adaptateur, et d’allumer également la broche grâce au bouton on/off.

##### 6 / Vitesse

Avant de découper on règle la vitesse de rotation en fonction du matériau et son épaisseur.

##### 7 / Découpe

Pendant la découpe, il nous suffit plus qu'à déplacer la machine le long du guide numérique, il faut avancer pas trop rapidement pour ne pas la perdre, dans la direction donner par la machine et garder le guide à l’intérieur du cercle blanc. Sinon la machine se rélève et on perd un peu de temps.

##### 7 / Résultat

![](screenmodule5/terre.jpg)
